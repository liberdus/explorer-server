# explorer-server

## Explorer server for the Shardus Network.

Main function of explorer server is to collect and index the important data and information of shardus network (at a regular interval) from archiver server and expose the data from APIs.

Explorer server use Fastify.js and developed using TypeScript. For data storage, it use `sqlite3` and `knex` an ORM.

Default port: 4444

### API Endpoints

- GET /
  explorer-client frontend

- GET /cycleinfo\
  return all cycles records in the network.

- GET /cycleinfo/:count\
  return latest of cycle records in which `count` is the number of cycles to be returned

### Data Models

#### Cycle

```
export interface Cycle {
  counter: number
  certificate: string
  previous: string
  marker: string
  start: number
  duration: number
  active: number
  desired: number
  expired: number
  syncing: number
  joined: string
  joinedArchivers: string
  joinedConsensors: string
  refreshedArchivers: string
  refreshedConsensors: string
  activated: string
  activatedPublicKeys: string
  removed: string
  returned: string
  lost: string
  refuted: string
  apoptosized: string
}
```

### How to start server

```
npm install
npm start
```
