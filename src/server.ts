require('dotenv').config();

import path = require('path');
import fastifyStatic from 'fastify-static';
import pointOfView from 'point-of-view';
import {Collector} from './class/Collector';
import * as ioclient from 'socket.io-client';
import * as Storage from './controller/storage';
import * as Fastify from 'fastify';
import * as crypto from 'shardus-crypto-utils';
import * as utils from './utils';
import fastifyCors from 'fastify-cors';
import {Server, IncomingMessage, ServerResponse} from 'http';
import {getTransactionStatus} from './controller/status';

crypto.init('69fa4195670576c0160d660c3be36556ff8d504725be8a59b5a96509e0c994bc');

// config variables
import {config as CONFIG} from './config';
if (process.env.PORT) {
  CONFIG.port = process.env.PORT;
}

interface RequestParams {
  counter: string;
}
interface RequestQuery {
  page: string;
  count: string;
  from: string;
  to: string;
  cycle: number;
  partition: number;
  txid: string;
}

console.log(`http://${CONFIG.archiverInfo.ip}:${CONFIG.archiverInfo.port}`);

const socketClient = ioclient.connect(
  `http://${CONFIG.archiverInfo.ip}:${CONFIG.archiverInfo.port}`
);

socketClient.on('connect', () => {
  console.log('connected to archive server');
});

// Setup Log Directory
const start = async () => {
  const clientModule = require.resolve('explorer-client');
  const clientDirectory = path.dirname(clientModule);
  const viewDirectory = path.join(clientDirectory + '/views');
  const staticDirectory = path.resolve(clientDirectory + '/public');

  console.log('absoluteClientPath', clientDirectory);
  console.log('view Directory', viewDirectory);
  console.log('static Directory', staticDirectory);

  await Storage.initStorage();
  const lastStoredCycles = await Storage.queryLatestCycleRecords(1);
  let lastStoredCycleCounter = 0;
  if (lastStoredCycles.length > 0) {
    lastStoredCycleCounter = lastStoredCycles[0].counter;
  }
  console.log('lastStoredCycleNumber', lastStoredCycleCounter);
  const collector = new Collector();

  const server: Fastify.FastifyInstance<
    Server,
    IncomingMessage,
    ServerResponse
  > = Fastify.fastify({
    logger: false,
  });

  const io = require('socket.io')(server.server);

  server.register(pointOfView, {
    engine: {
      ejs: require('ejs'),
    },
    templates: viewDirectory,
  });

  server.register(fastifyStatic, {
    root: staticDirectory,
    prefix: '/public/',
  });

  server.register(fastifyCors);

  server.get('/', (req, reply: any) => {
    reply.view('/index.ejs');
  });

  // all cycles page
  server.get('/cycle', (req, reply: any) => {
    reply.view('/cycles.ejs');
  });

  // all txs page
  server.get('/transaction', (req, reply: any) => {
    reply.view('/transactions.ejs');
  });

  // cycle detail page
  server.get('/cycle/:counter', (req, reply: any) => {
    reply.view('/cycledetail.ejs');
  });

  // cycle detail page
  server.get('/partition', (req, reply: any) => {
    reply.view('/partition.ejs');
  });

  server.get('/api/cycleinfo', async (_request, reply) => {
    const err = utils.validateTypes(_request.query, {
      count: 's?',
      to: 's?',
      from: 's?',
    });
    if (err) {
      reply.send({success: false, error: err});
      return;
    }
    const query = _request.query as RequestQuery;
    let archivedCycles = [];
    if (query.count) {
      let count: number = parseInt(query.count);
      if (count <= 0 || Number.isNaN(count)) {
        reply.send({success: false, error: 'Invalid count'});
        return;
      }
      if (count > 100) count = 100; // set to show max 100 cycles
      archivedCycles = await Storage.queryAllArchivedCycles(count);
    } else if (query.to && query.from) {
      const from = parseInt(query.from);
      const to = parseInt(query.to);
      if (
        !(from >= 0 && to >= from) ||
        Number.isNaN(from) ||
        Number.isNaN(to)
      ) {
        console.log('Invalid start and end counters for cycleinfo');
        reply.send({
          success: false,
          error: 'Invalid from and to counter for cycleinfo',
        });
        return;
      }
      archivedCycles = await Storage.queryAllArchivedCyclesBetween(from, to);
      console.log('archivedCycles', archivedCycles);
    } else {
      reply.send({success: false, error: 'not specified which cycle to show'});
      return;
    }
    const res = {
      success: true,
      cycles: archivedCycles.map((a: any) => a.cycleRecord),
      // cycles: archivedCycles,
    };
    reply.send(res);
  });

  server.get('/api/transaction', async (_request, reply) => {
    const err = utils.validateTypes(_request.query, {
      count: 's?',
      page: 's?',
      txid: 's?',
    });
    if (err) {
      reply.send({success: false, error: err});
      return;
    }
    const query = _request.query as RequestQuery;
    const itemsPerPage = 10;
    let totalPages = 0;
    let transactions;
    if (query.count) {
      const count: number = parseInt(query.count);
      //max 1000 transactions
      if (count > 1000) {
        reply.send({success: false, error: 'The count number is too big.'});
        return;
      } else if (count <= 0 || Number.isNaN(count)) {
        reply.send({success: false, error: 'Invalid count'});
        return;
      }
      transactions = await await Storage.queryTransactions(0, count);
    } else if (query.page) {
      const page: number = parseInt(query.page);
      if (page <= 0 || Number.isNaN(page)) {
        reply.send({success: false, error: 'Invalid page number'});
        return;
      }
      //checking totalPages first
      const dataBlobs = await Storage.queryTxBlobs();
      const totalTransactions = dataBlobs.reduce((p, c) => p + c.totalTx, 0);
      totalPages = Math.ceil(totalTransactions / itemsPerPage);
      if (page > totalPages) {
        reply.send({
          success: false,
          error: 'Page no is greater than the totalPage',
        });
      }
      transactions = await Storage.queryTransactions(
        (page - 1) * itemsPerPage,
        itemsPerPage
      );
    } else if (query.txid) {
      transactions = await Storage.queryTransactionById(query.txid);
    } else {
      reply.send({
        success: false,
        error: 'not specified which transaction to show',
      });
      return;
    }
    const res: any = {
      success: true,
      transactions: transactions,
    };
    if (query.page) {
      res.totalPages = totalPages;
    }
    reply.send(res);
  });

  server.get('/api/cycleinfo/:counter', async (_request, reply) => {
    const err = utils.validateTypes(_request.params, {counter: 's'});
    if (err) {
      reply.send({success: false, error: err});
      return;
    }
    const params = _request.params as RequestParams;
    const counter: number = parseInt(params.counter);
    //cycle counter starts from 0
    if (counter < 0 || Number.isNaN(counter)) {
      reply.send({success: false, error: 'Invalid counter'});
      return;
    }
    const archivedCycle = await Storage.queryArchivedCycleByCounter(counter);
    const res = {
      success: true,
      cycle: archivedCycle.cycleRecord,
    };
    reply.send(res);
  });

  server.get('/api/archive/:counter', async (_request, reply) => {
    const err = utils.validateTypes(_request.params, {counter: 's'});
    if (err) {
      reply.send({success: false, error: err});
      return;
    }
    const params = _request.params as RequestParams;
    const counter: number = parseInt(params.counter);
    //cycle counter starts from 0
    if (counter < 0 || Number.isNaN(counter)) {
      reply.send({success: false, error: 'Invalid counter'});
      return;
    }
    const archivedCycle = await Storage.queryArchivedCycleByCounter(counter);
    const res = {
      success: true,
      archivedCycle,
    };
    reply.send(res);
  });

  server.post('/api/tx/status', async (_request, reply) => {
    const transactionInfo: any = _request.body;
    if (
      !transactionInfo.txid ||
      !transactionInfo.timestamp ||
      !transactionInfo.address
    ) {
      console.log('No txid or timestamp or address provided');
      reply.send({
        success: false,
        error: 'No txid or timestamp or address provided',
      });
      return;
    }

    const err = utils.validateTypes(transactionInfo, {
      txid: 's',
      timestamp: 'n',
      address: 's',
    });
    if (err) {
      reply.send({success: false, error: err});
      return;
    }

    const result = await getTransactionStatus(
      utils.short(transactionInfo.txid),
      transactionInfo.timestamp,
      transactionInfo.address
    );

    if (!result) {
      reply.send({
        success: false,
        result: 'Transaction not found',
      });
    }

    reply.send({
      success: true,
      result,
    });
  });

  server.get('/api/summary/account', async (_request, reply) => {
    const dataBlobs = await Storage.queryDataBlobs();
    const res = {
      success: true,
      blobs: dataBlobs,
    };
    reply.send(res);
  });

  server.get('/api/summary/tx', async (_request, reply) => {
    const txBlobs = await Storage.queryTxBlobs();
    const res = {
      success: true,
      blobs: txBlobs,
    };
    reply.send(res);
  });

  io.on('connection', () => {
    console.log('Connected');
  });

  socketClient.on('ARCHIVED_CYCLE', async (data: any) => {
    console.log('RECEIVED ARCHIVED_CYCLE');
    try {
      collector.processData(data);
    } catch (e) {
      console.log('Error in processing received data!', e);
    }
  });

  server.listen(Number(CONFIG.port), '0.0.0.0', err => {
    if (err) {
      server.log.error(err);
      console.log(err);
      throw err;
    }
    console.log('Liberdus explorer server is listening on port:', CONFIG.port);
  });
};

start();
