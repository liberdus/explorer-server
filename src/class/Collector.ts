import * as crypto from 'shardus-crypto-utils';
import * as Storage from '../controller/storage';
import * as utils from '../utils';
import {config as CONFIG} from '../config';

export interface Data {
  archivedCycles: Storage.ArchivedCycle[];
  sign: {
    owner: string;
    sig: string;
  };
}

export class Collector {
  constructor() {}

  async processData(data: Data) {
    let err = utils.validateTypes(data, {sign: 'o', archivedCycles: 'a'});
    if (err) {
      return;
    }
    err = utils.validateTypes(data.sign, {owner: 's', sig: 's'});
    if (err) {
      return;
    }
    // the archiverinfo is hard-coded at the moment
    if (data.sign.owner !== CONFIG.archiverInfo.publicKey) {
      console.log('Data received from archive-server has invalid key');
      return;
    }
    if (!crypto.verifyObj(data)) {
      console.log('Data received from archive-server has invalid signature');
      return;
    }
    if (!data.archivedCycles) {
      console.log('Data received from archive-server is invalid');
      return;
    }
    const {archivedCycles} = data;
    // [TODO] validate sender of this data
    for (const archivedCycle of archivedCycles) {
      // [TODO] better interface check for received archived cycle
      if (!archivedCycle.cycleRecord || !archivedCycle.cycleMarker) {
        console.log('Invalid Archived Cycle Received', archivedCycle);
        return;
      }
      if (archivedCycle.data) {
        const archivedCycleData = archivedCycle.data;
        if (
          !archivedCycleData.parentCycle ||
          !archivedCycleData.networkHash ||
          !archivedCycleData.partitionHashes
        ) {
          console.log(
            'The data field in ArchivedCycle is invalid',
            archivedCycleData
          );
          return;
        }
        if (
          typeof archivedCycleData.parentCycle !== 'string' ||
          typeof archivedCycleData.networkHash !== 'string' ||
          typeof archivedCycleData.partitionHashes !== 'object'
        ) {
          console.log(
            'The data field in ArchivedCycle is invalid',
            archivedCycleData
          );
          return;
        }
      }
      if (archivedCycle.receipt) {
        const archivedCycleReceipt = archivedCycle.receipt;
        if (
          !archivedCycleReceipt.parentCycle ||
          !archivedCycleReceipt.networkHash ||
          !archivedCycleReceipt.partitionHashes ||
          !archivedCycleReceipt.partitionMaps ||
          !archivedCycleReceipt.partitionMaps
        ) {
          console.log(
            'The receipt field in ArchivedCycle is invalid',
            archivedCycleReceipt
          );
          return;
        }
        if (
          typeof archivedCycleReceipt.parentCycle !== 'string' ||
          typeof archivedCycleReceipt.networkHash !== 'string' ||
          typeof archivedCycleReceipt.partitionHashes !== 'object' ||
          typeof archivedCycleReceipt.partitionMaps !== 'object' ||
          typeof archivedCycleReceipt.partitionTxs !== 'object'
        ) {
          console.log(
            'The receipt field in ArchivedCycle is invalid',
            archivedCycleReceipt
          );
          return;
        }
      }
      if (archivedCycle.summary) {
        const archivedCycleSummary = archivedCycle.summary;
        if (
          !archivedCycleSummary.parentCycle ||
          !archivedCycleSummary.networkHash ||
          !archivedCycleSummary.partitionHashes ||
          !archivedCycleSummary.partitionBlobs
        ) {
          console.log(
            'The receipt field in ArchivedCycle is invalid',
            archivedCycleSummary
          );
          return;
        }
        if (
          typeof archivedCycleSummary.parentCycle !== 'string' ||
          typeof archivedCycleSummary.networkHash !== 'string' ||
          typeof archivedCycleSummary.partitionHashes !== 'object' ||
          typeof archivedCycleSummary.partitionBlobs !== 'object'
        ) {
          console.log(
            'The summary field in ArchivedCycle is invalid',
            archivedCycleSummary
          );
          return;
        }
      }
      const existingArchivedCycle = await Storage.queryArchivedCycleByMarker(
        archivedCycle.cycleMarker
      );
      if (existingArchivedCycle) {
        await Storage.updateArchivedCycle(
          archivedCycle.cycleMarker,
          archivedCycle
        );
      } else {
        await Storage.insertArchivedCycle(archivedCycle);
      }
    }
  }
}
