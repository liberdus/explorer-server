export const config = {
  env: process.env.NODE_ENV || 'development',
  host: process.env.HOST || '127.0.0.1',
  port: process.env.PORT || '5555',
  archiverInfo: {
    ip: '127.0.0.1',
    port: '4000',
    publicKey:
      '758b1c119412298802cd28dbfa394cdfeecc4074492d60844cc192d632d84de3',
  },
  EXPLORER_DB: 'explorer-db',
};
