import {arch} from 'os';
import * as Storage from './storage';
import { StateManager } from 'shardus-types'
export function calculateHomePartititon(
  address: string,
  numPartitions: number
) {
  const addressNum = parseInt(address.slice(0, 8), 16);

  // 2^32
  const size = Math.round((0xffffffff + 1) / numPartitions);
  let homePartition = Math.floor(addressNum / size);

  if (homePartition === numPartitions) {
    homePartition = homePartition - 1;
  }

  return homePartition;
}

export async function convertToCycleAndPartition(
  timestamp: number,
  address: string
) {
  const foundCycle = await Storage.queryCyclesByTimestamp(
    Math.round(timestamp / 1000)
  );
  if (foundCycle) {
    const {counter, networkId, active} = foundCycle.cycleRecord;
    const partition = calculateHomePartititon(address, active);
    return {
      cycle: counter,
      partition,
      networkId,
    };
  }
  return {
    cycle: null,
    partition: null,
  };
}

export function lookUpReceiptInReceiptMap(
  txid: string,
  receiptMap: StateManager.StateManagerTypes.ReceiptMap
) {
  if (receiptMap[txid]) return receiptMap[txid];
  return false;
}

export async function getTransactionStatus(
  txid: string,
  timestamp: number,
  address: string
): Promise<string[] | null | undefined> {
  const {cycle, partition, networkId} = await convertToCycleAndPartition(
    timestamp,
    address
  );
  if (!cycle || partition === null || partition === undefined) return null;
  const archivedCycle = await Storage.queryArchivedCycleByCounter(cycle);
  if (!archivedCycle) return;
  if (!archivedCycle.receipt) return;
  const receiptsInPartition = archivedCycle.receipt.partitionMaps[partition];
  if (!receiptsInPartition) return;
  if (receiptsInPartition[txid]) {
    return receiptsInPartition[txid];
  }
  return null;
}
