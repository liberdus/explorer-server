/* eslint-disable no-empty */
import {Cycle} from './cycle';
import {Database, BaseModel, FS_Persistence_Adapter} from 'tydb';
import {config} from '../config/index';
import {StateManager} from 'shardus-types';

export let Collection: any;

export class ArchivedCycle extends BaseModel {
  cycleRecord!: Cycle;
  cycleMarker!: StateManager.StateMetaDataTypes.CycleMarker;
  data!: StateManager.StateMetaDataTypes.StateData;
  receipt!: StateManager.StateMetaDataTypes.Receipt;
  summary!: StateManager.StateMetaDataTypes.Summary;
}

export function isArchivedCycle(obj: ArchivedCycle): obj is ArchivedCycle {
  return (
    (obj as ArchivedCycle).cycleRecord !== undefined &&
    (obj as ArchivedCycle).cycleMarker !== undefined
  );
}

export const initStorage = async () => {
  Collection = new Database<ArchivedCycle>({
    ref: config.EXPLORER_DB,
    model: ArchivedCycle,
    persistence_adapter: FS_Persistence_Adapter,
    autoCompaction: 10 * 30 * 1000, // database compaction every 10 cycles
  });
  await Collection.createIndex({fieldName: 'cycleMarker', unique: true});
};

export async function insertArchivedCycle(archivedCycle: any) {
  try {
    await Collection.insert([ArchivedCycle.new(archivedCycle)]);
    console.log(
      'Successfully inserted archivedCycle',
      archivedCycle.cycleRecord.counter,
      archivedCycle.cycleRecord.marker
    );
  } catch (e) {
    console.log(e);
    console.log(
      'Unable to insert archive cycle or it is already stored in to database',
      archivedCycle.cycleRecord.counter,
      archivedCycle.cycleMarker
    );
  }
}

export async function updateArchivedCycle(marker: string, archivedCycle: any) {
  await Collection.update({
    filter: {cycleMarker: marker},
    update: {$set: archivedCycle},
  });
  console.log(
    'Updated archived cycle for cycle',
    archivedCycle.cycleRecord.counter,
    archivedCycle.cycleMarker
  );
}

export async function queryAllArchivedCycles(count?: number) {
  const archivedCycles = await Collection.find({
    filter: {},
    sort: {
      'cycleRecord.counter': -1,
    },
    limit: count ? count : null,
    project: {
      _id: 0,
    },
  });
  return archivedCycles;
}

export async function queryAllArchivedCyclesBetween(
  start: number,
  end: number
) {
  const archivedCycles = await Collection.find({
    filter: {
      $and: [
        {'cycleRecord.counter': {$gte: start}},
        {'cycleRecord.counter': {$lte: end}},
      ],
    },
    sort: {
      'cycleRecord.counter': -1,
    },
    limit: 100,
    project: {
      _id: 0,
    },
  });

  return archivedCycles;
}

export async function queryAllCycleRecords() {
  const cycleRecords = await Collection.find({
    filter: {},
    sort: {
      'cycleRecord.counter': -1,
    },
    project: {
      _id: 0,
      cycleMarker: 0,
      receipt: 0,
      data: 0,
      summary: 0,
    },
  });
  return cycleRecords.map((item: any) => item.cycleRecord);
}

export async function queryLatestCycleRecords(count = 1) {
  const cycleRecords = await Collection.find({
    filter: {},
    sort: {
      'cycleRecord.counter': -1,
    },
    limit: count,
    project: {
      _id: 0,
      cycleMarker: 0,
      receipt: 0,
      data: 0,
      summary: 0,
    },
  });
  return cycleRecords.map((item: any) => item.cycleRecord);
}

export async function queryTxBlobs() {
  const summaries = await Collection.find({
    filter: {},
    sort: {
      'cycleRecord.counter': -1,
    },
    project: {
      _id: 0,
      cycleMarker: 0,
      receipt: 0,
      data: 0,
      cycleRecord: 0,
    },
  });
  const blobsForEachCycle = summaries
    .filter((s: any) => s.summary)
    .map((s: any) => s.summary.partitionBlobs);
  const txBlobs = [];
  for (const blobByCycle of blobsForEachCycle) {
    const blobs: any = Object.values(blobByCycle);
    for (const b of blobs) {
      if (b.opaqueBlob.totalTx) {
        txBlobs.push({
          latestCycle: b.latestCycle,
          partition: b.partition,
          totalTx: b.opaqueBlob.totalTx,
          txByType: b.opaqueBlob.txByType,
        });
      }
    }
  }
  return txBlobs;
}

export async function queryDataBlobs() {
  const summaries = await Collection.find({
    filter: {},
    limit: 10,
    sort: {
      'cycleRecord.counter': -1,
    },
    project: {
      _id: 0,
      cycleMarker: 0,
      receipt: 0,
      data: 0,
    },
  });
  const blobsForEachCycle = summaries
    .filter((s: any) => s.summary)
    .map((s: any) => s.summary.partitionBlobs);
  const blobsForLatestCycle = blobsForEachCycle[1];
  const dataBlobs = [];
  if (blobsForEachCycle && blobsForLatestCycle) {
    const blobs: any = Object.values(blobsForLatestCycle);
    for (const b of blobs) {
      if (b.opaqueBlob.totalAccounts) {
        dataBlobs.push({
          latestCycle: b.latestCycle,
          partition: b.partition,
          totalAccounts: b.opaqueBlob.totalAccounts,
          totalBalance: b.opaqueBlob.totalBalance,
          accByType: b.opaqueBlob.accByType,
        });
      }
    }
  }
  return dataBlobs;
}

export async function queryCycleRecordsBetween(start: number, end: number) {
  const cycleRecords = await Collection.find({
    filter: {
      $and: [
        {'cycleRecord.counter': {$gte: start}},
        {'cycleRecord.counter': {$lte: end}},
      ],
    },
    sort: {
      'cycleRecord.counter': -1,
    },
  });
  return cycleRecords.map((item: any) => item.cycleRecord);
}

export async function queryArchivedCycleByMarker(marker: string) {
  const archivedCycles = await Collection.find({
    filter: {cycleMarker: marker},
  });
  if (archivedCycles.length > 0) return archivedCycles[0];
}

export async function queryArchivedCycleByCounter(counter: number) {
  const archivedCycles = await Collection.find({
    filter: {'cycleRecord.counter': counter},
  });
  if (archivedCycles.length > 0) return archivedCycles[0];
}

export const queryCyclesByTimestamp = async (timestamp: number) => {
  //TODO need to limit 1
  const data = await Collection.find({
    filter: {'cycleRecord.start': {$lte: timestamp}},
    sort: {
      'cycleRecord.counter': -1,
    },
  });
  if (data.length > 0) return data[0];
};

export async function queryTransactions(offset = 0, limit = 10) {
  let archivedCycles = await Collection.find({
    filter: {},
    sort: {
      'cycleRecord.counter': -1,
    },
    project: {
      _id: 0,
      cycleMarker: 0,
      data: 0,
      summary: 0,
    },
  });
  const txsById: any = {};
  const allTxs = [];
  let skip = 0;
  const skipTxs: string[] = [];
  archivedCycles = archivedCycles.filter((r: any) => r.receipt);
  for (const eachArchivedCycle of archivedCycles) {
    for (const partition in eachArchivedCycle.receipt.partitionMaps) {
      const receiptsInPartition =
        eachArchivedCycle.receipt.partitionMaps[partition];
      for (const txId in receiptsInPartition) {
        const txObj = {
          txId,
          status: receiptsInPartition[txId],
          cycle: eachArchivedCycle.cycleRecord.counter,
          partition,
        };
        if (offset && skip < offset) {
          if (!skipTxs.includes(txId)) {
            skipTxs.push(txId);
            skip += 1;
          }
          continue;
        }
        if (skipTxs.includes(txId)) break;
        if (txsById[txId]) {
          txsById[txId].push(txObj);
        }
        if (allTxs.length >= limit) break;
        if (!txsById[txId]) {
          allTxs.push(txObj);
          txsById[txId] = [txObj];
        }
      }
    }
  }
  return {txsById, allTxs};
}

export async function queryTransactionById(txId: string) {
  let archivedCycles = await Collection.find({
    filter: {},
    sort: {
      'cycleRecord.counter': -1,
    },
    project: {
      _id: 0,
      cycleMarker: 0,
      data: 0,
      summary: 0,
    },
  });
  archivedCycles = archivedCycles.filter((r: any) => r.receipt);
  for (const eachArchivedCycle of archivedCycles) {
    for (const partition in eachArchivedCycle.receipt.partitionMaps) {
      const receiptsInPartition =
        eachArchivedCycle.receipt.partitionMaps[partition];
      if (receiptsInPartition[txId]) {
        return {
          txId,
          status: receiptsInPartition[txId],
          cycle: eachArchivedCycle.cycleRecord.counter,
          partition,
        };
      }
    }
  }
  return null;
}
