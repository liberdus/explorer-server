import { P2P } from 'shardus-types'

export interface Cycle extends P2P.CycleCreatorTypes.CycleRecord {
  certificate: string;
  marker: string;
}
